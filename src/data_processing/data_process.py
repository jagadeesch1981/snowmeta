import json
import src.data_processing.core.functions as library_function
from snowflake.snowpark import Session
from src.data_processing.utils.logr import sf_logger, set_logger, get_logs_list, set_framework_logger, fw_logger
from src.data_processing.utils.utils import get_config, update_job_status, send_email, get_args, create_session, \
    log_str_size_check
from datetime import datetime
import sys


def function_executor(ss: Session,
                      process_name: str,
                      conf: dict,
                      runtime_params: dict,
                      restart_run_id: int = None,
                      restart_step_id: int = None
                      ):
    """
    Executes functions specified in pipeline config for the process `process_name`

    Parameters:
    ss (Session): A Snowpark session
    process_name (str): The name of the process to be executed
    conf (dict): A dictionary of configurations
    restart_run_id (str, optional): A unique identifier for a previous run, used for restarting a run

    Returns:
    None
    """
    # Generate unique run id
    run_id: int = int(datetime.now().strftime('%Y%m%d%H%M%S%f')[:-3])
    fw_logger.info(f"Starting function executor for process name: {process_name} and run ID: {run_id}")
    sf_logger.info(f"Process name: {process_name}")
    sf_logger.info("run ID: {}".format(run_id))

    # Retrieve pipeline config from database
    meta_db = conf['snowflake']['database']
    meta_schema = conf['snowflake']['schema']

    job_status_args = {}
    pipeline = ''
    pipeline_dict = {}
    job_status = ''
    df_dict = {'start_time': datetime.now().strftime("%Y-%m-%d %H:%M:%S"), 'run_id': run_id}
    df_dict.update(conf)
    df_dict.update(runtime_params)

    try:
        pipeline = \
            ss.table(f"{meta_db}.{meta_schema}.pipeline") \
                .where(f"process_name = '{process_name}'") \
                .first().as_dict()['PIPELINE_CONFIG']
        pipeline_dict = json.loads(pipeline)
        sf_logger.info("Pipeline config: {}".format(pipeline_dict))

        # Insert record into the job status table
        update_job_status(ss=ss,
                          process_name=process_name,
                          action='insert',
                          pipeline_config=json.dumps(pipeline_dict).replace("'", "''"),
                          run_id=run_id,
                          restart_run_id=restart_run_id,
                          database=meta_db,
                          schema=meta_schema)

        # Job status dict for 'update' status
        job_status_args.update({
            'ss': ss,
            'process_name': process_name,
            'action': 'update',
            'pipeline_config': pipeline,
            'run_id': run_id,
            'restart_run_id': restart_run_id,
            'database': meta_db,
            'schema': meta_schema,
            'meta_wh': conf['snowflake']['warehouse']
            # ,'meta_role': conf['snowflake']['role']
        })

        # Check if pipeline is started in `restart` mode
        if restart_run_id:
            sf_logger.info(f"Pipeline {process_name} has been started in restart mode. "
                           f"Failed run_id is {restart_run_id}")
            df_dict.update({'restart_run_id': restart_run_id})
            df_dict.update({'restart_flag': True})

            if restart_step_id:
                sf_logger.info(f'Pipeline will start executing from step id {restart_step_id}')
                df_dict.update({'restart_step_id': restart_step_id})
        else:
            sf_logger.info(f'pipeline `{process_name}` has been started.')

        # Get dataflow from pipeline config
        dataflow = pipeline_dict['root']['data_flow']

        # Set Data context for Data processing
        library_function.ddl(ss=ss, func_args=pipeline_dict.get('root').get('data_context'))

        # Loop through functions in dataflow and execute them
        for func_dict in dataflow:
            if df_dict.get('stream_has_data', True):
                # Check step ID with restart_step_id to handle the restart from specific step.
                # In normal start restart_step_id defaults to '000'
                if int(restart_step_id) <= int(func_dict['step_id']):
                    sf_logger.info("Function dictionary: {}".format(func_dict))
                    func_args = {k: v for k, v in func_dict.items() if k != 'func'}
                    func = getattr(library_function, func_dict['func'])
                    func_output = func(ss, func_args, df_dict)

                    # update df_dict if output is dataframe
                    if func_output is not None:
                        df_dict.update(func_output)
                    else:
                        sf_logger.info("No output to update dataframe dictionary")
                else:
                    sf_logger.info(f"Restart mode. "
                                   f"Skipping step ID `{func_dict['step_id']}` and function `{func_dict['func']}`")
            else:
                fw_logger.info(f"Skipping step ID `{func_dict['step_id']}` and function `{func_dict['func']}`")

        # Once data processing done switch framework [database, schema] and warehouse to perform this tasks
        sf_logger.info(f'pipeline `{process_name}` has been completed successfully.')

        # Update 'SUCCEEDED' status in job status table
        job_status = 'SUCCEEDED'
        job_status_args.update({'pipeline_status': job_status, 'log_msg_str': log_str_size_check(get_logs_list())})
        update_job_status(**job_status_args)
        # else:
        #     raise Exception(f"Process name {process_name} does not exist in pipeline table.")
    except Exception as e:
        import traceback
        sf_logger.error(traceback.format_exc())
        sf_logger.error(f'pipeline `{process_name}` has been failed.')

        # Update 'FAILED' status in job status table
        job_status = 'FAILED'
        if pipeline:
            job_status_args.update({'pipeline_status': job_status, 'log_msg_str': log_str_size_check(get_logs_list())})
            update_job_status(**job_status_args)
        else:
            pass
        sys.exit(1)
    finally:
        try:
            sf_logger.info(f'sending `{process_name}` email notification.')

            email_intg_name = pipeline_dict.get('root', {}).get('email_intg_name', 'jagadeesch.sf@gmail.com')
            email_addr_list = pipeline_dict.get('root', {}).get('email_addr_list', ['jagadeesch.sf@gmail.com'])

            func_args = {'status': job_status,
                         'process_name': process_name,
                         'start_time': df_dict.get('start_time'),
                         'email_intg_name': email_intg_name,
                         'email_addr_list': email_addr_list,
                         'total_records_processed': df_dict.get('total_records_processed', '0'),
                         'failed_record_count': df_dict.get('failed_record_count', '0'),
                         'run_id': run_id
                         }

            send_email(ss, func_args)
            ss.close()

        except Exception as e:
            sf_logger.warning(f'email notification failed with {e}. '
                              f'Please check status in `pipeline` table for run_id : `{run_id}` for more details')

    fw_logger.info("Exiting function executor for process name: {}".format(process_name))


# Entry point of the script
if __name__ == "__main__":
    # Parse Commandline arguments
    params = get_args()

    # Setting log level for framework
    set_framework_logger(params.log_level.upper())

    fw_logger.info("Starting Data Processing component")
    fw_logger.info(f"Framework log leve is set to `{params.log_level.upper()}`")

    # Read configuration file (from S3)
    fw_logger.info(f"Reading configuration file `{params.config_file}`")
    pipeline_config = get_config(params.config_file)['config']

    # Set logging level
    fw_logger.info(f"Setting Snowpark log level to `{pipeline_config['log_level']}`")
    set_logger(pipeline_config['log_level'])

    # Create Snowpark session
    fw_logger.info(f"Calling Snowpark Session creation")

    if 'snowflake' in pipeline_config:
        session = create_session(pipeline_config['snowflake'])
    else:
        raise Exception(f"Snowflake configuration not found in config file {params.config_file} ")

    # Check runtime params passed
    rt_params = json.loads(params.runtime_params) if params.runtime_params else {}

    # Call function executor with the session, process name, configuration
    # and restart run ID, restart step ID in case of restart
    fw_logger.info(f"Calling function executor")
    function_executor(ss=session,
                      process_name=params.process_name,
                      conf=pipeline_config,
                      runtime_params=rt_params,
                      restart_run_id=params.restart_run_id,
                      restart_step_id=params.restart_step_id
                      )

    fw_logger.info(f"End of the Data Processing component")
